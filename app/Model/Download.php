<?php
App::uses('AppModel', 'Model');
/**
 * Download Model
 *
 * @property DownloadFile $DownloadFile
 */
class Download extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique' => array(
				'rule' => 'isUnique',
				'message' => 'Already Used'
			)
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $hasOne = array(
		'DownloadFile' => array(
			'className' => 'DownloadFile',
			'foreignKey' => 'download_id',
			'conditions' => array(
				'DownloadFile.active' => true
			),
			'fields' => '',
			'order' => ''
		)
	);

	public function getActiveDownload($name=null) {
		return $this->findByName($name);
	}
}
