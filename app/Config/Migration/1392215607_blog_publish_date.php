<?php
class BlogPublishDate extends CakeMigration {

/**
 * Migration description
 *
 * @var string
 * @access public
 */
	public $description = '';

/**
 * Actions to be performed
 *
 * @var array $migration
 * @access public
 */
	public $migration = array(
		'up' => array(
			'create_field' => array(
				'blogs' => array(
					'publish_date' => array('type' => 'date', 'null' => false, 'default' => NULL, 'key' => 'index', 'after' => 'user_id'),
					'indexes' => array(
						'publish_date' => array('column' => 'publish_date', 'unique' => 0),
					),
				),
			),
		),
		'down' => array(
			'drop_field' => array(
				'blogs' => array('publish_date', 'indexes' => array('publish_date')),
			),
		),
	);

/**
 * Before migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function before($direction) {
		return true;
	}

/**
 * After migration callback
 *
 * @param string $direction, up or down direction of migration process
 * @return boolean Should process continue
 * @access public
 */
	public function after($direction) {
		return true;
	}
}
