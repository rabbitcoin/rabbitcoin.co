<?php

Router::mapResources(array('User'));
Router::parseExtensions();

Router::connect('/', array('controller' => 'home', 'action' => 'index'));
Router::connect('/about', array('controller' => 'home', 'action' => 'about'));
Router::connect('/team', array('controller' => 'home', 'action' => 'team'));
Router::connect('/links', array('controller' => 'home', 'action' => 'links'));
Router::connect('/blog', array('controller' => 'home', 'action' => 'blog'));
Router::connect('/blog/:id', array(
	'controller' => 'home',
	'action' => 'blog_detail'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));
Router::connect('/contact', array('controller' => 'home', 'action' => 'contact'));

## ADMIN
Router::connect('/admin/blogs/add', array('controller' => 'admin', 'action' => 'blogs_add'));
Router::connect('/admin/blogs/edit/:id', array(
	'controller' => 'admin',
	'action' => 'blogs_edit'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));
Router::connect('/admin/blogs/delete/:id', array(
	'controller' => 'admin',
	'action' => 'blogs_delete'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));

Router::connect('/admin/links/add', array('controller' => 'admin', 'action' => 'links_add'));
Router::connect('/admin/links/edit/:id', array(
	'controller' => 'admin',
	'action' => 'links_edit'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));
Router::connect('/admin/links/delete/:id', array(
	'controller' => 'admin',
	'action' => 'links_delete'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));

Router::connect('/admin/linkGroups/add', array('controller' => 'admin', 'action' => 'link_groups_add'));
Router::connect('/admin/linkGroups/edit/:id', array(
	'controller' => 'admin',
	'action' => 'link_groups_edit'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));
Router::connect('/admin/linkGroups/delete/:id', array(
	'controller' => 'admin',
	'action' => 'link_groups_delete'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));

Router::connect('/admin/users/add', array('controller' => 'admin', 'action' => 'users_add'));
Router::connect('/admin/users/edit/:id', array(
	'controller' => 'admin',
	'action' => 'users_edit'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));
Router::connect('/admin/users/delete/:id', array(
	'controller' => 'admin',
	'action' => 'users_delete'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));

Router::connect('/admin/downloads/add', array('controller' => 'admin', 'action' => 'downloads_add'));
Router::connect('/admin/downloads/edit/:id', array(
	'controller' => 'admin',
	'action' => 'downloads_edit'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));
Router::connect('/admin/downloads/delete/:id', array(
	'controller' => 'admin',
	'action' => 'downloads_delete'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));

Router::connect('/admin/downloads/file/add/:id', array(
	'controller' => 'admin',
	'action' => 'downloads_file_add'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));
Router::connect('/admin/downloads/file/delete/:id', array(
	'controller' => 'admin',
	'action' => 'downloads_file_delete'
), array(
	'pass' => array('id'),
	'id' => '[0-9]+'
));

CakePlugin::routes();

require CAKE . 'Config' . DS . 'routes.php';
