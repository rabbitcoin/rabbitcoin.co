<div class="gut lnk">

	<h3>Related Links</h3>

	The following are things created by our team and our amazing community. If you have a link you'd like added, please <a href="mailto:rabbitcointeam@gmail.com">email us</a> so we can get it
	added!
	<br/><br/>

	<?php $linkGroup = ''; ?>
	<div class="gut lnk">
		<?php
		foreach($links as $link) {
			if($link['LinkGroup']['title'] != $linkGroup) {
				if(!empty($linkGroup)) {
					echo '</ul>';
				}
				$linkGroup = $link['LinkGroup']['title'];
				echo sprintf("<h4>%s</h4>", $linkGroup);

				echo '<ul class="list-unstyled">';
			}

			echo '<li>' . $this->Html->link($link['Link']['title'], $link['Link']['href']) . '</li>';
		}

		echo '</ul>';
		?>
	</div>
	
</div>