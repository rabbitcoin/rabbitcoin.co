<div class="row homepage">
	
	<img src="/theme/RabbitCoin/images/home_logo.png" class="home_logo" alt="RabbitCoin" />
	<br/>

	<div class="downloads">

		<?php if (time() < strtotime($launch_time)) { ?>
			<div id="clock"></div>
			<script>
				$('#clock').countdown('<?php echo $launch_time; ?>').on('update.countdown', function(event) {
					var $this = $(this).html(event.strftime(''
					+ '<span>%-d</span> Day%!d, '
					+ '<span>%-H</span> Hour%!H, '
					+ '<span>%-M</span> Minute%!M, '
					+ '<span>%-S</span> Second%!S'));
				}).on('finish.countdown', function(event) {
					$('#clock').fadeOut("slow", function() { $('#download_now').fadeIn(); });
				});
			</script>
			<div id="download_now" style="display:none;"><a href="/?launch"><div class="download">Download Now</div></a></div>
		<?php } else { ?>
			<a href="<?php echo $file_windows['DownloadFile']['href']; ?>"><div class="download"><?php echo $file_windows['Download']['title']; ?> <span class="ver"><?php echo $file_windows['DownloadFile']['version']; ?></span></div></a>
			<a href="<?php echo $file_mac['DownloadFile']['href']; ?>"><div class="download"><?php echo $file_mac['Download']['title']; ?> <span class="ver"><?php echo $file_mac['DownloadFile']['version']; ?></span></div></a>
			<a href="<?php echo $file_source['DownloadFile']['href']; ?>"><div class="download"><?php echo $file_source['Download']['title']; ?> <span class="ver"><?php echo $file_source['DownloadFile']['version']; ?></span></div></a>
		<?php } ?>

	</div>
</div>