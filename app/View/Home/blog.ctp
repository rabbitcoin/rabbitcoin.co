<div class="gut lnk">

	<h3>Our Blog</h3>

	<div class="gut lnk">
		<?php
		foreach($blogs as $blog) {
			?>
			<a href="/blog/<?php echo $blog['Blog']['id']; ?>"><b><?php echo $blog['Blog']['title']; ?></b></a><br/>
			<?php echo $blog['Blog']['body']; ?>
			<br/>
			<div class="timestamp"><?php echo date("m/d/Y", strtotime($blog['Blog']['created'])); ?></div>
			<br/><br/>
			<?php 
		}
		?>
	</div>
	
</div>