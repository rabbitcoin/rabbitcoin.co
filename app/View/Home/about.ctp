
<div class="gut">

	<h3>About Rab<b>bit</b></h3>

	Rab<b>bit</b> is an experiment in fast-hashing blocks combined with the Kimoto Gravity Well technology. Using the same reward and block parameters as Dogecoin, we've increased the
	block find and retarget rates to reach a quicker end-of-life cycle while preventing large multipool abuse with KGW difficulty recalculating.
	<br/><br/>

	Algorithm: Scrypt<br/>
	Block Time: 30 seconds<br/>
	Difficulty Refactor: KGW<br/>
	Max Coins: 100 Billion<br/><br/>
	Reward: (Random)<br/>
	Block 1-100,000: 1-1,000,000 RabbitCoin<br/>
	Block 100,001-200,000: 1-500,000 RabbitCoin<br/>
	Block 200,001-300,000: 1-250,000 RabbitCoin<br/>
	Block 300,001-400,000: 1-125,000 RabbitCoin<br/>
	Block 400,001-500,000: 1-62,500 RabbitCoin<br/>
	Block 500,001-600,000: 1-31,250 RabbitCoin<br/>
	Block 600,000+: 10,000 (Flat) RabbitCoin<br/>
	
	<br/><br/>
	
	<div class="row">
		 <div class="col-xs-12 client_ss">
		 	<img class="col-xs-12 client_img" src="/theme/RabbitCoin/images/client.png" />
		 </div>
	</div>

	<br/>
	Questions, comments or interested in helping? Please <a href="/contact">contact us</a> - we'd love to have you on our team.

</div>
