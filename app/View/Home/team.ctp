<div class="gut">

	<h3>Our Team</h3>

	We're a group of friends who just happen to be well wrapped up in the technology world. Here's a few people behind the core efforts of CorgiCoin.
	<br/><br/>

	<div class="row team_row">
		<div class="col-md-4"><img class="col-md-12" src="/theme/CorgiCoin/images/team_dustin.png" /></div>
		<div class="col-md-8">
			<h4>Dustin Vannatter <span class="team_title">Team Lead</span></h4>
			Dustin develops and thinks of things for a living. He also writes a lot of stuff and spends most of his time chasing his 5 kids and corgi around.
			<br/><br/>
			<div class="team_links">
				<div class="team_link"><a target="_new" href="http://twitter.com/dustinvannatter"><img src="/theme/CorgiCoin/images/twitter.png" border="0" alt="Twitter" /></a></div>
				<div class="team_link"><a target="_new" href="http://dustin.io/"><img src="/theme/CorgiCoin/images/link.png" border="0" alt="Homepage" /></a></div>
				<div class="team_link"><a target="_new" href="http://linkedin.com/in/vannatter"><img src="/theme/CorgiCoin/images/linkedin.png" border="0" alt="LinkedIn" /></a></div>
			</div>
		</div>
	</div>	

	<div class="row team_row">
		<div class="col-md-4"><img class="col-md-12" src="/theme/CorgiCoin/images/team_jon.png" /></div>
		<div class="col-md-8">
			<h4>Jon Tomlinson <span class="team_title">Developer</span></h4>
			Jon writes code for a job and for fun - something is wrong with him. He also likes pugs and gnomes entirely too much. It's honestly a bit creepy.
			<br/><br/>
			<div class="team_links">
				<div class="team_link"><a target="_new" href="http://twitter.com/jontomlinson"><img src="/theme/CorgiCoin/images/twitter.png" border="0" alt="Twitter" /></a></div>
				<div class="team_link"><a target="_new" href="http://jtomlinson.net/"><img src="/theme/CorgiCoin/images/link.png" border="0" alt="Homepage" /></a></div>
				<div class="team_link"><a target="_new" href="http://linkedin.com/in/jontomlinson"><img src="/theme/CorgiCoin/images/linkedin.png" border="0" alt="LinkedIn" /></a></div>
			</div>
		</div>
	</div>

	<div class="row team_row">
		<div class="col-md-4"><img class="col-md-12" src="/theme/CorgiCoin/images/team_taber.png" /></div>
		<div class="col-md-8">
			<h4>Taber Buhl <span class="team_title">UI/UX</span></h4>
			Taber has been doing UI development forever. He has a couple of small, angry dogs. He is also really good at stalling a BMW in a Taco Bell drive-thru. 
			<br/><br/>
			<div class="team_links">
				<div class="team_link"><a target="_new" href="http://twitter.com/taberbuhl"><img src="/theme/CorgiCoin/images/twitter.png" border="0" alt="Twitter" /></a></div>
				<div class="team_link"><a target="_new" href="http://taberbuhl.com/"><img src="/theme/CorgiCoin/images/link.png" border="0" alt="Homepage" /></a></div>
				<div class="team_link"><a target="_new" href="http://linkedin.com/in/taber"><img src="/theme/CorgiCoin/images/linkedin.png" border="0" alt="LinkedIn" /></a></div>
			</div>
		</div>
	</div>

	<br/><br/>
	If you need help, you can also <a href="mailto:corgicoin@gmail.com">contact our team</a>.

</div>