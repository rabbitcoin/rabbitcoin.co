
<div class="gut">

	<h3>Contact Us</h3>

	Comments, suggestions or need help? Try one of these resources to get help from the development team or from our amazing community.
	<br/><br/>
	
	<a href="http://twitter.com/rabbitcointeam">@rabbitcointeam twitter</a><br/>
	<a href="http://reddit.com/r/rabbitcoin/">/r/rabbitcoin subreddit</a><br/>
	<a href="mailto:rabbitcointeam@gmail.com">rabbitcointeam@gmail.com</a><br/>
	
</div>