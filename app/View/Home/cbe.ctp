
<div class="gut">

	<h3>CBE - CorgiCoin Block Explorer</h3>

	CBE is an updated port of the Python-based project ABE. We will add more information and share our updates once we have a stable build that we can comfortably share with the
	community.
	<br/><br/>
	Interested in helping with CBE development? Please <a href="/contact_us">contact us</a> - we'd love to add you to our team!

</div>
