<?php echo $this->Html->docType('html5'); ?>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		RabbitCoin - Admin:
		<?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<?php
	echo $this->Html->meta('icon');
	echo $this->fetch('meta');
	echo $this->Html->css('bootstrap.min');
	echo $this->Html->css('datepicker3');
	echo $this->Html->css('admin');

	echo $this->fetch('css');
	echo $this->Html->script('libs/jquery-1.10.2.min');
	echo $this->Html->script('libs/bootstrap.min');
	echo $this->Html->script('libs/jquery.hotkeys');
	echo $this->Html->script('libs/bootstrap-wysiwyg');
	echo $this->Html->script('libs/bootstrap-datepicker');
	echo $this->Html->script('common');
	echo $this->Html->script('respond.min');
	echo $this->fetch('script');
	?>
	<link href="//netdna.bootstrapcdn.com/bootswatch/3.1.0/flatly/bootstrap.min.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css'>
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', '<?php echo Configure::read('Settings.API.google_analytics.key'); ?>', 'corgicoin.com');
		ga('send', 'pageview');
	</script>
</head>

<body>
<div id="main-container">
	<div id="content" class="container">
		<div class="row">
			<?php echo $this->element('menu/top_nav_admin'); ?>
		</div>
		<div class="row">
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
		</div>
	</div>
</div>
</body>
</html>