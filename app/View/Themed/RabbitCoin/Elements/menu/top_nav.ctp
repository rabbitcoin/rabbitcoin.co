<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a href="/" class="navbar-brand">Rab<span class="rabbit_bold">bit</span></a>
	</div>
	<div class="collapse navbar-collapse navbar-ex1-collapse main_nav">
		<ul class="nav navbar-nav">
			<li><a href="/about">About</a></li>
			<li><a href="https://prohashing.com/explorer/Rabbitcoin/" target="_new">Explorer</a></li>
			<li><a href="/links">Links</a></li>
			<li><a href="/contact">Contact</a></li>
			<li><a href="/blog">Blog</a></li>
		</ul>
	</div>
</nav>