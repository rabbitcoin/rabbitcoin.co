<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button><!-- /.navbar-toggle -->
		<a href="/" class="navbar-brand">Rabbit</a>
	</div>
	<div class="collapse navbar-collapse navbar-ex1-collapse main_nav">
		<ul class="nav navbar-nav">
			<li><a href="/admin">admin</a></li>
			<li><a href="/admin/users">users</a></li>
			<li><a href="/admin/blogs">blog</a></li>
			<li class="dropdown">
				<a href="/admin/links" class="dropdown-toggle" data-toggle="dropdown">links <b class="caret"></b></a>
				<ul class="dropdown-menu">
					<li><a href="/admin/links">edit links</a></li>
					<li><a href="/admin/linkGroups">link groups</a></li>
				</ul>
			</li>
			<li><a href="/admin/downloads">builds / downloads</a></li>
		</ul>
	</div>
</nav>