<?php echo $this->Html->docType('html5'); ?> 
<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>RabbitCoin - <?php echo $title_for_layout; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<?php if(@$rss) { ?>
		<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php echo $rss; ?>" />
		<?php } ?>

		<?php
			echo $this->Html->meta('icon');
			echo $this->fetch('meta');
			echo $this->Html->css('bootstrap.min');
			echo $this->Html->css('override');

			echo $this->fetch('css');
			echo $this->Html->script('libs/jquery-1.10.2.min');
			echo $this->Html->script('libs/bootstrap.min');
			echo $this->Html->script('countdown');
			echo $this->Html->script('respond.min');
			echo $this->Html->script('common');
			echo $this->fetch('script');
		?>
		<link href='http://fonts.googleapis.com/css?family=Raleway:600,800' rel='stylesheet' type='text/css'>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		  ga('create', 'UA-48086330-1', 'rabbitco.in');
  		  ga('send', 'pageview');
		</script>
	</head>

	<body>
		<div id="main-container">
			<div id="content" class="container">
				<div class="row">
					<?php echo $this->element('menu/top_nav'); ?>
				</div>
				<div class="row">
					<?php echo $this->Session->flash(); ?>
					<?php echo $this->fetch('content'); ?>
				</div>
			</div>
			<div id="footer" class="container">
				<?php echo $this->element('menu/bot_nav'); ?>
			</div>
		</div>
	</body>
</html>