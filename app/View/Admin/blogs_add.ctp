<div>
	<?php echo $this->Form->create('Blog', array(
		'inputDefaults'=> array(
			'div' => 'form-group',
			'label' => array(
				'class' => 'col col-md-3 control-label'
			),
			'wrapInput' => 'col col-md-9',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal'
	)); ?>
	<?php echo $this->Form->input('body', array('type' => 'hidden')); ?>
	<?php echo $this->Form->input('title', array('placeHolder' => 'Title')); ?>
	<div class="form-group">
		<label class="col col-md-3 control-label">Body</label>
		<div class="col col-md-9">
			<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">

				<div class="btn-group">
					<a class="btn btn-default btn-sm" data-edit="formatBlock p" data-original-title="Paragraph" title="Paragraph">P</a>
					<a class="btn btn-default btn-sm" data-edit="formatBlock h2" data-original-title="Header" title="Header">H1</a>
					<a class="btn btn-default btn-sm" data-edit="formatBlock h3" data-original-title="2nd-level header" title="2nd-level header">H2</a>
					<a class="btn btn-default btn-sm" data-edit="formatBlock h4" data-original-title="3rd-level header" title="3rd-level header">H3</a>
				</div>

				<div class="btn-group">
					<a class="btn btn-default btn-sm" data-edit="bold" data-original-title="Bold (Ctrl/Cmd+B)" title="Bold"><b>B</b></a>
					<a class="btn btn-default btn-sm" data-edit="italic" data-original-title="Italic (Ctrl/Cmd+I)" title="Italic"><i>I</i></a>
					<a class="btn btn-default btn-sm" data-edit="underline" data-original-title="Underline (Ctrl/Cmd+U)" title="Underline"><span style="text-decoration: underline;">UL</span></a>
				</div>

				<div class="btn-group">
					<a class="btn btn-default btn-sm" data-edit="insertunorderedlist" data-original-title="Bullet list" title="Bullet list"><span class="glyphicon glyphicon-list"></span></a>
					<a class="btn btn-default btn-sm" data-edit="insertorderedlist" data-original-title="Number list" title="Number list"><span class="glyphicon glyphicon-list-alt"></span></a>
					<a class="btn btn-default btn-sm" data-edit="outdent" data-original-title="Reduce indent (Shift+Tab)" title="Reduce indent"><span class="glyphicon glyphicon-indent-left"></span></a>
					<a class="btn btn-default btn-sm" data-edit="indent" data-original-title="Indent (Tab)" title="Indent"><span class="glyphicon glyphicon-indent-right"></span></a>
				</div>

				<div class="btn-group">
					<a class="btn btn-default btn-sm" data-edit="justifyleft" data-original-title="Align Left (Ctrl/Cmd+L)" title="Align left"><span class="glyphicon glyphicon-align-left"></span></a>
					<a class="btn btn-default btn-sm" data-edit="justifycenter" data-original-title="Center (Ctrl/Cmd+E)" title="Center"><span class="glyphicon glyphicon-align-center"></span></a>
					<a class="btn btn-default btn-sm" data-edit="justifyright" data-original-title="Align Right (Ctrl/Cmd+R)" title="Align right"><span class="glyphicon glyphicon-align-right"></span></a>
					<a class="btn btn-default btn-sm" data-edit="justifyfull" data-original-title="Justify (Ctrl/Cmd+J)" title="Justify"><span class="glyphicon glyphicon-align-justify"></span></a>
				</div>

				<div class="btn-group">
					<a class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" data-original-title="Hyperlink" title="Hyperlink"><span class="glyphicon glyphicon-link"></span></a>
					<div class="dropdown-menu input-append">
						<input class="span2" placeholder="URL" type="text" data-edit="createLink">
						<button class="btn" type="button">Add</button>
					</div>
					<a class="btn btn-default btn-sm" data-edit="unlink" data-original-title="Remove Hyperlink" title="Remove hyperlink"><span class="glyphicon glyphicon-remove-circle"></span></a>
				</div>

				<div class="btn-group">
					<a class="btn btn-default btn-sm" data-edit="undo" data-original-title="Undo (Ctrl/Cmd+Z)" title="Undo"><span class="glyphicon glyphicon-backward"></span></a>
					<a class="btn btn-default btn-sm" data-edit="redo" data-original-title="Redo (Ctrl/Cmd+Y)" title="Redo"><span class="glyphicon glyphicon-forward"></span></a>
				</div>

			</div>
			<div id="editor"></div>
		</div>
	</div>
	<?php echo $this->Form->input('publish_date', array('type' => 'text', 'class' => 'date form-control col-xs-3', 'wrapInput' => 'col col-xs-3')); ?>
	<div class="form-group">
		<?php echo $this->Form->submit(__('Save Entry'), array('div' => 'col col-md-9 col-md-offset-3', 'class' => 'btn btn-primary')); ?>
	</div>
</div>
<script>
	$('a[title]').tooltip({container:'body'});

	$('.dropdown-menu input').click(function() {
		return false;
	}).change(function () {
		$(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle');
	}).keydown('esc', function () {
		this.value='';$(this).change();
	});

	$('[data-role=magic-overlay]').each(function () {
		var overlay = $(this), target = $(overlay.data('target'));
		overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
	});

	$('#editor').wysiwyg().html($('#BlogBody').val());

	$('#BlogBlogsEditForm, #BlogBlogsAddForm').submit(function() {
		var editor = $('#editor');

		editor.cleanHtml();

		$('#BlogBody').val(editor.html());
	});

	$('.date').datepicker({
		format: 'yyyy-mm-dd'
	});
</script>