<table class="table table-striped">
	<thead>
	<tr>
		<th><?php echo $this->Paginator->sort('LinkGroup.id', __('ID')); ?></th>
		<th><?php echo $this->Paginator->sort('LinkGroup.title', __('Title')); ?></th>
		<th><?php echo $this->Paginator->sort('LinkGroup.modified', __('Modified')); ?></th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($linkGroups as $group) { ?>
		<tr>
			<td><?php echo $group['LinkGroup']['id']; ?></td>
			<td><?php echo $group['LinkGroup']['title']; ?></td>
			<td><?php echo $group['LinkGroup']['modified']; ?></td>
			<td>
				<?php echo $this->Html->link(__('Edit'), sprintf('/admin/linkGroups/edit/%s', $group['LinkGroup']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
				<?php echo $this->Html->link(__('Delete'), sprintf('/admin/linkGroups/delete/%s', $group['LinkGroup']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure?')); ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div class="well">
	<?php echo $this->Html->link(__('Add Link Group'), '/admin/linkGroups/add', array('class' => 'btn btn-primary')); ?>
</div>