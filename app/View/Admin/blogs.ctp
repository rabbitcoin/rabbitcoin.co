<table class="table table-striped">
	<thead>
	<tr>
		<th><?php echo $this->Paginator->sort('Blog.title', __('Title')); ?></th>
		<th><?php echo $this->Paginator->sort('Blog.publish_date', __('Publish Date')); ?></th>
		<th><?php echo $this->Paginator->sort('Blog.modified', __('Modified')); ?></th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($blogs as $blog) { ?>
		<tr>
			<td><?php echo $blog['Blog']['title']; ?></td>
			<td><?php echo $blog['Blog']['publish_date']; ?></td>
			<td><?php echo $blog['Blog']['modified']; ?></td>
			<td>
				<?php echo $this->Html->link(__('Edit'), sprintf('/admin/blogs/edit/%s', $blog['Blog']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
				<?php echo $this->Html->link(__('Delete'), sprintf('/admin/blogs/delete/%s', $blog['Blog']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure?')); ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div class="well">
	<?php echo $this->Html->link(__('Add Blog'), '/admin/blogs/add', array('class' => 'btn btn-primary')); ?>
</div>