<table class="table table-striped">
	<thead>
	<tr>
		<th><?php echo $this->Paginator->sort('Download.name', __('Name')); ?></th>
		<th><?php echo $this->Paginator->sort('Download.title', __('Title')); ?></th>
		<th><?php echo $this->Paginator->sort('DownloadFile.version', __('Version')); ?></th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($downloads as $download) { ?>
		<tr>
			<td><?php echo $download['Download']['name']; ?></td>
			<td><?php echo $download['Download']['title']; ?></td>
			<td><?php echo $download['DownloadFile']['version']; ?></td>
			<td>
				<?php echo $this->Html->link(__('Edit'), sprintf('/admin/downloads/edit/%s', $download['Download']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
				<?php echo $this->Html->link(__('Delete'), sprintf('/admin/downloads/delete/%s', $download['Download']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure?')); ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div class="well">
	<?php echo $this->Html->link(__('Add Download'), '/admin/downloads/add', array('class' => 'btn btn-primary')); ?>
</div>