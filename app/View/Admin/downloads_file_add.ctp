<div>
	<?php echo $this->Form->create('DownloadFile', array(
		'inputDefaults'=> array(
			'div' => 'form-group',
			'label' => array(
				'class' => 'col col-md-3 control-label'
			),
			'wrapInput' => 'col col-md-9',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal',
		'type' => 'file'
	)); ?>
	<?php echo $this->Form->input('title', array('placeHolder' => 'Title')); ?>
	<?php echo $this->Form->input('href', array('placeHolder' => 'HREF', 'afterInput' => '<span class="help-block">Don\'t select a file if you include a HREF</span>')); ?>
	<?php echo $this->Form->input('file', array('type' => 'file', 'wrapInput' => 'col-xs-3')); ?>
	<?php echo $this->Form->input('version', array('placeHolder' => 'Version')); ?>
	<?php echo $this->Form->input('active', array('before' => '<label class="col col-md-3 control-label">Active</label>', 'label' => false, 'class' => false)); ?>
	<div class="form-group">
		<?php echo $this->Form->submit(__('Save Download'), array('div' => 'col col-md-9 col-md-offset-3', 'class' => 'btn btn-primary')); ?>
	</div>
</div>