<div class="col-md-6 col-md-offset-3" style="margin-top: 50px;">
	<?php echo $this->Form->create('User', array(
		'inputDefaults'=> array(
			'div' => 'form-group',
			'label' => array(
				'class' => 'col col-md-3 control-label'
			),
			'wrapInput' => 'col col-md-9',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal'
	)); ?>
	<?php echo $this->Form->input('email', array('placeHolder' => 'Email Address')); ?>
	<?php echo $this->Form->input('password', array('placeHolder' => 'Password')); ?>
	<div class="form-group">
		<?php echo $this->Form->submit(__('Create User'), array('div' => 'col col-md-9 col-md-offset-3', 'class' => 'btn btn-primary')); ?>
	</div>
</div>