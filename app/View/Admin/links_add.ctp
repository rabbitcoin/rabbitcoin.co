<div>
	<?php echo $this->Form->create('Link', array(
		'inputDefaults'=> array(
			'div' => 'form-group',
			'label' => array(
				'class' => 'col col-md-3 control-label'
			),
			'wrapInput' => 'col col-md-9',
			'class' => 'form-control'
		),
		'class' => 'form-horizontal'
	)); ?>
	<?php
	$afterInput = '<span class="help-block">' . $this->Html->link(__('New Link Group'), '/admin/linkGroups/add?redirect=' . $this->here) . '</span>';

	if(@$this->request->query['groupId']) {
		$groupId = $this->request->query['groupId'];
	} else {
		$groupId = @$this->request->data['Link']['link_group_id'];
	}
	?>
	<?php echo $this->Form->input('link_group_id', array('afterInput' => $afterInput, 'value' => $groupId)); ?>
	<?php echo $this->Form->input('href', array('placeHolder' => 'HREF')); ?>
	<?php echo $this->Form->input('title', array('placeHolder' => 'Title')); ?>
	<?php echo $this->Form->input('description', array('placeHolder' => 'Description')); ?>
	<div class="form-group">
		<?php echo $this->Form->submit(__('Save Link'), array('div' => 'col col-md-9 col-md-offset-3', 'class' => 'btn btn-primary')); ?>
	</div>
</div>