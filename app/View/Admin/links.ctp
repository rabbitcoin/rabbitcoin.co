<table class="table table-striped">
	<thead>
	<tr>
		<th><?php echo $this->Paginator->sort('Link.title', __('Title')); ?></th>
		<th><?php echo $this->Paginator->sort('LinkGroup.title', __('Link Group')); ?></th>
		<th><?php echo $this->Paginator->sort('Link.modified', __('Modified')); ?></th>
		<th>&nbsp;</th>
	</tr>
	</thead>
	<tbody>
	<?php foreach($links as $link) { ?>
		<tr>
			<td><?php echo $this->Html->link($link['Link']['title'], sprintf('/admin/links/edit/%s', $link['Link']['id'])); ?></td>
			<td><?php echo $link['LinkGroup']['title']; ?></td>
			<td><?php echo $link['Link']['modified']; ?></td>
			<td>
				<?php echo $this->Html->link(__('Edit'), sprintf('/admin/links/edit/%s', $link['Link']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
				<?php echo $this->Html->link(__('Delete'), sprintf('/admin/links/delete/%s', $link['Link']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure?')); ?>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>
<div class="well">
	<?php echo $this->Html->link(__('Add Link'), '/admin/links/add', array('class' => 'btn btn-primary')); ?>
</div>