<div class="col-xs-6 col-md-4">
	<h2><?php echo __('Download'); ?></h2>
	<div class="well well-sm">
		<?php echo $this->Form->create('Download', array(
			'inputDefaults'=> array(
				'div' => 'form-group',
				'label' => array(
					'class' => 'col col-md-3 control-label'
				),
				'wrapInput' => 'col col-md-9',
				'class' => 'form-control'
			),
			'class' => 'form-horizontal'
		)); ?>
		<?php echo $this->Form->input('title', array('placeHolder' => 'Title')); ?>
		<?php echo $this->Form->input('name', array('placeHolder' => 'Name')); ?>
		<div class="form-group">
			<?php echo $this->Form->submit(__('Save Download'), array('div' => 'col col-md-9 col-md-offset-3', 'class' => 'btn btn-primary')); ?>
		</div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>

<div class="col-xs-12 col-md-8">
	<h2><?php echo __('Files'); ?></h2>
	<div class="well well-sm">
		<table class="table table-striped">
			<thead>
			<tr>
				<th><?php echo __('Active'); ?></th>
				<th><?php echo __('File'); ?></th>
				<th><?php echo __('Version'); ?></th>
				<th>&nbsp;</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach($files as $file) { ?>
				<tr>
					<td><?php echo $this->Form->checkbox(sprintf('active', $file['DownloadFile']['id']), array('data-file-id' => $file['DownloadFile']['id'], 'data-download-id' => $this->request->data['Download']['id'], 'class' => 'fileActive', 'checked' => $file['DownloadFile']['active'])); ?></td>
					<td>
						<?php
						if(empty($file['DownloadFile']['filename'])) {
							## offsite link
							$title = $file['DownloadFile']['href'];
							if(strlen($title) > 50) {
								$title = substr($title, 0, 46) . '...';
							}
						} else {
							## local file
							$title = $file['DownloadFile']['filename'];
						}

						echo $title;
						?>
					</td>
					<td><?php echo $file['DownloadFile']['version']; ?></td>
					<td>
						<?php echo $this->Html->link(__('Delete'), sprintf('/admin/downloads/file/delete/%s', $file['DownloadFile']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure?')); ?>
					</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		<div class="well">
			<?php echo $this->Html->link(__('Add File'), sprintf('/admin/downloads/file/add/%s', $this->request->data['Download']['id']), array('class' => 'btn btn-primary')); ?>
		</div>
	</div>
</div>
<script>
	$('.fileActive').change(function() {
		var chk = $(this);
		var fileId = chk.attr('data-file-id');
		var downloadId = chk.attr('data-download-id');

		chk.attr('disabled', true);

		$('.fileActive').prop('checked', false);
		chk.prop('checked', true);

		$.getJSON('/admin/setActiveFile', { fileId: fileId, downloadId: downloadId }, function(data) {
			if(data.error) {
				alert(data.message);
			}

			chk.attr('disabled', false);
		});
	});
</script>