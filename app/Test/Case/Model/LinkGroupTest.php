<?php
App::uses('LinkGroup', 'Model');

/**
 * LinkGroup Test Case
 *
 */
class LinkGroupTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.link_group',
		'app.link',
		'app.user',
		'app.blog'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->LinkGroup = ClassRegistry::init('LinkGroup');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->LinkGroup);

		parent::tearDown();
	}

}
