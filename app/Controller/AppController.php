<?php

App::uses('Controller', 'Controller');
App::uses('Sanitize', 'Utility');
/**
 * CakePHP Component & Model Code Completion
 * @author junichi11
 *
 * ==============================================
 * CakePHP Core Components
 * ==============================================
 * @property AuthComponent $Auth
 * @property AclComponent $Acl
 * @property CookieComponent $Cookie
 * @property EmailComponent $Email
 * @property RequestHandlerComponent $RequestHandler
 * @property SecurityComponent $Security
 * @property SessionComponent $Session
 */

class AppController extends Controller {

	public $theme = "RabbitCoin";

    public $components = array(
        'Session'
    );

	public $helpers = array(
		'Common'
	);

    public function beforeFilter() {
    }
    
}
