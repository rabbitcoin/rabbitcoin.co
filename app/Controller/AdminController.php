<?php
/**
 * @property User $User
 * @property Blog $Blog
 * @property Link $Link
 * @property LinkGroup $LinkGroup
 * @property Download $Download
 */

class AdminController extends AppController {
	var $uses = array('User', 'Blog', 'Link', 'LinkGroup', 'Download');
	var $helpers = array(
		'Session',
		'Html' => array('className' => 'BoostCake.BoostCakeHtml'),
		'Form' => array('className' => 'BoostCake.BoostCakeForm'),
		'Paginator' => array('className' => 'BoostCake.BoostCakePaginator'),
	);
	var $components = array(
		'Session',
		'Auth' => array(
			'loginAction' => array(
				'controller' => 'admin',
				'action' => 'login'
			),
			'loginRedirect' => array('controller' => 'admin', 'action' => 'index'),
			'logoutRedirect' => array('controller' => 'admin', 'action' => 'login'),
			'autoRedirect' => false,
			'authenticate' => array(
				'Form' => array(
					'fields' => array('username' => 'email'),
					'passwordHasher' => 'Blowfish',
				),
			),
		),
	);
	public $paginate = array(
		'Blog' => array(
			'limit' => 20,
			'order' => array('created' => 'desc'),
		),
		'User' => array(
			'limit' => 20,
			'order' => array('modified' => 'desc'),
		),
		'Link' => array(
			'limit' => 20,
			'order' => array('modified' => 'desc'),
		),
		'LinkGroup' => array(
			'limit' => 20,
			'order' => array('modified' => 'desc'),
		),
		'Download' => array(
			'limit' => 20,
			'order' => array('modified' => 'desc'),
		)
	);

	public function beforeFilter() {
		parent::beforeFilter();

		$this->Auth->allow('addUser');   ## this should only be exposed to add the inital user, then removed

		$this->layout = 'admin';
	}

	public function index() {
		$this->set('title_for_layout', 'Admin');
	}

	public function blogs() {
		$this->set('title_for_layout', 'Blogs');

		$this->Blog->recursive = 0;
		$this->set('blogs', $this->paginate('Blog'));
	}

	public function blogs_add() {
		if($this->request->is('post')) {
			$this->Blog->data = $this->request->data;

			$this->Blog->data['Blog']['user_id'] = $this->Auth->user('id');

			if($this->Blog->save()) {
				$this->Session->setFlash(__('Blog Entry Saved'), 'flash/success');
				$this->redirect('/admin/blogs');
			}
		}
	}

	public function blogs_edit($id=null) {
		if($this->request->is('post') || $this->request->is('put')) {
			$this->Blog->data = $this->request->data;

			$this->Blog->data['Blog']['id'] = $id;
			$this->Blog->data['Blog']['user_id'] = $this->Auth->user('id');

			$this->Blog->id = $id;

			if($this->Blog->save()) {
				$this->Session->setFlash(__('Blog Entry Saved'), 'flash/success');
				$this->redirect('/admin/blogs');
			}
		} else {
			if($blog = $this->Blog->findById($id)) {
				$this->request->data = $blog;
			} else {
				$this->Session->setFlash(__('Invalid Blog Entry'), 'flash/error');
				$this->redirect('/admin/blogs');
			}
		}

		$this->render('blogs_add');
	}

	public function blogs_delete($id=null) {
		$this->Blog->id = $id;
		if(!$this->Blog->exists()) {
			throw new NotFoundException(__('Invalid Blog Entry'));
		}

		if($this->Blog->delete()) {
			$this->Session->setFlash(__('Blog Entry Deleted'), 'flash/success');
			$this->redirect('/admin/blogs');
		} else {
			$this->Session->setFlash(__('Blog Entry Not Deleted'), 'flash/error');
			$this->redirect('/admin/blogs');
		}
	}

	public function links() {
		$this->set('title_for_layout', 'Links');

		$this->Link->recursive = 0;
		$this->set('links', $this->paginate('Link'));
	}

	public function links_add() {
		if($this->request->is('post')) {
			$this->Link->data = $this->request->data;

			$this->Link->data['Link']['user_id'] = $this->Auth->user('id');

			if($this->Link->save()) {
				$this->Session->setFlash(__('Link Saved'), 'flash/success');
				$this->redirect('/admin/links');
			}
		}

		$this->set('linkGroups', $this->LinkGroup->find('list'));
	}

	public function links_edit($id=null) {
		$this->Link->id = $id;
		if(!$this->Link->exists()) {
			throw new NotFoundException(__('Invalid Link'));
		}

		if($this->request->is('put')) {
			$this->Link->data = $this->request->data;

			$this->Link->data['Link']['id'] = $id;
			$this->Link->data['Link']['user_id'] = $this->Auth->user('id');

			$this->Link->id = $id;
			if($this->Link->save()) {
				$this->Session->setFlash(__('Link Saved'), 'flash/success');
				$this->redirect('/admin/links');
			}
		} else {
			$this->request->data = $this->Link->read();
		}

		$this->set('linkGroups', $this->LinkGroup->find('list'));

		$this->render('links_add');
	}

	public function links_delete($id=null) {
		$this->Link->id = $id;
		if(!$this->Link->exists()) {
			throw new NotFoundException(__('Invalid Link'));
		}

		if($this->Link->delete()) {
			$this->Session->setFlash(__('Link Deleted'), 'flash/success');
			$this->redirect('/admin/links');
		} else {
			$this->Session->setFlash(__('Link Not Deleted'), 'flash/error');
			$this->redirect('/admin/links');
		}
	}

	public function linkGroups() {
		$this->set('title_for_layout', 'Link Groups');

		$this->User->recursive = 0;
		$this->set('linkGroups', $this->paginate('LinkGroup'));
	}

	public function link_groups_add() {
		if($this->request->is('post')) {
			$this->LinkGroup->data = $this->request->data;

			if($this->LinkGroup->save()) {
				$this->Session->setFlash(__('Link Group Saved'), 'flash/success');

				if(@$this->request->query['redirect']) {
					$this->redirect(sprintf('%s?groupId=%s', $this->request->query['redirect'], $this->LinkGroup->id));
				} else {
					$this->redirect('/admin/linkGroups');
				}
			}
		}
	}

	public function link_groups_edit($id=null) {
		$this->LinkGroup->id = $id;
		if(!$this->LinkGroup->exists()) {
			throw new NotFoundException(__('Invalid Link Group'));
		}

		if($this->request->is('put')) {
			$this->LinkGroup->data = $this->request->data;

			$this->LinkGroup->data['LinkGroup']['id'] = $id;

			$this->LinkGroup->id = $id;
			if($this->LinkGroup->save()) {
				$this->Session->setFlash(__('Link Group Saved'), 'flash/success');
				$this->redirect('/admin/linkGroups');
			}
		} else {
			$this->request->data = $this->LinkGroup->read();
		}

		$this->render('link_groups_add');
	}

	public function link_groups_delete($id=null) {
		$this->LinkGroup->id = $id;
		if(!$this->LinkGroup->exists()) {
			throw new NotFoundException(__('Invalid Link Group'));
		}

		if($this->LinkGroup->delete()) {
			$this->Session->setFlash(__('Link Group Deleted'), 'flash/success');
			$this->redirect('/admin/linkGroups');
		} else {
			$this->Session->setFlash(__('Link Group Not Deleted'), 'flash/error');
			$this->redirect('/admin/linkGroups');
		}
	}

	public function users() {
		$this->set('title_for_layout', 'Users');

		$this->User->recursive = 0;
		$this->set('users', $this->paginate('User'));
	}

	public function users_add() {
		if($this->request->is('post')) {
			$this->User->data = $this->request->data;

			if($this->User->save()) {
				$this->Session->setFlash(__('User Saved'), 'flash/success');
				$this->redirect('/admin/users');
			}
		}
	}

	public function users_edit($id=null) {
		if($this->request->is('put')) {
			$this->User->data = $this->request->data;

			$this->User->data['User']['id'] = $id;

			$this->User->id = $id;

			if($this->User->save()) {
				$this->Session->setFlash(__('User Saved'), 'flash/success');
				$this->redirect('/admin/users');
			}
		} else {
			if($user = $this->User->findById($id)) {
				$this->request->data = $user;
			} else {
				$this->Session->setFlash(__('Invalid User'), 'flash/error');
				$this->redirect('/admin/users');
			}
		}

		$this->render('users_add');
	}

	public function users_delete($id=null){
		$this->User->id = $id;
		if(!$this->User->exists()) {
			throw new NotFoundException(__('Invalid User'));
		}

		if($this->User->delete()) {
			$this->Session->setFlash(__('User Deleted'), 'flash/success');
			$this->redirect('/admin/users');
		} else {
			$this->Session->setFlash(__('User Not Deleted'), 'flash/error');
			$this->redirect('/admin/users');
		}
	}

	public function login() {
		$this->set('title_for_layout', 'Login');

		if($this->request->is('post')) {
			if($this->Auth->login()) {
				$this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Session->setFlash(__('Email or Password invalid'), 'flash/error');
			}
		}
	}

	public function addUser() {
		## only allow this function, if there isn't already a user created
		if(!$user = $this->User->find('first')) {
			if($this->request->is('post')) {
				## save the user, if we can
				$this->User->data = $this->request->data;

				if($this->User->save()) {
					$this->Session->setFlash(__('User Created'), 'flash/success');
					$this->redirect('/admin');
				}
			}
		} else {
			$this->Session->setFlash(__('User already exists, can\'t add'), 'flash/error');
			$this->redirect('/');
		}
	}

	public function downloads() {
		$this->set('title_for_layout', 'Builds / Downloads');

		$this->Download->recursive = 0;
		$this->set('downloads', $this->paginate('Download'));
	}

	public function downloads_add() {
		if($this->request->is('post')) {
			$this->Download->data = $this->request->data;

			if($this->Download->save()) {
				$this->Session->setFlash(__('Download Saved'), 'flash/success');
				$this->redirect(sprintf('/admin/downloads/edit/%s', $this->Download->id));
			}
		}
	}

	public function downloads_edit($id=null) {
		$this->Download->id = $id;
		if(!$this->Download->exists()) {
			throw new NotFoundException(__('Invalid Download'));
		}

		if($this->request->is('post') || $this->request->is('put')) {
			$this->Download->data = $this->request->data;

			if($this->Download->save()) {
				$this->Session->setFlash(__('Updated'), 'flash/success');
				$this->redirect(sprintf('/admin/downloads/edit/%s', $id));
			}
		} else {
			$this->request->data = $this->Download->read();
		}

		$this->set('files', $this->Download->DownloadFile->find('all', array('conditions' => array('DownloadFile.download_id' => $id))));
	}

	public function downloads_delete($id=null) {
		$this->Download->id = $id;
		if(!$this->Download->exists()) {
			throw new NotFoundException(__('Download Not Found'));
		}

		$this->Download->delete($id, true);

		$this->Download->DownloadFile->deleteAll(array('DownloadFile.download_id' => $id));

		$this->Session->setFlash(__('Download and all files deleted'), 'flash/success');
		$this->redirect($this->referer());
	}

	public function downloads_file_add($downloadId=null) {
		$this->Download->id = $downloadId;
		if(!$this->Download->exists()) {
			throw new NotFoundException(__('Invalid Download'));
		}

		if($this->request->is('post') || $this->request->is('put')) {
			$data = $this->request->data;

			if(@$data['DownloadFile']['href'] && !empty($data['DownloadFile']['href'])) {
				## offsite link
			} else {
				## uploaded file?
			}

			$data['DownloadFile']['download_id'] = $downloadId;

			$this->Download->DownloadFile->data = $data;

			if($this->Download->DownloadFile->save()) {
				if($data['DownloadFile']['active']) {
					## mark all files, for this download, as inactive
					$this->Download->DownloadFile->updateAll(array('DownloadFile.active' => false), array('DownloadFile.download_id' => $downloadId, 'DownloadFile.id !=' => $this->Download->DownloadFile->id));
				}
				$this->Session->setFlash(__('File Added'), 'flash/success');
				$this->redirect(sprintf('/admin/downloads/edit/%s', $downloadId));
			}
		}
	}

	public function downloads_file_delete($id=null) {
		$this->Download->DownloadFile->id = $id;
		if(!$this->Download->DownloadFile->exists()) {
			throw new NotFoundException(__('File Not Found'));
		}

		$this->Download->DownloadFile->delete($id);

		$this->Session->setFlash(__('File Deleted'), 'flash/success');
		$this->redirect($this->referer());
	}

	public function setActiveFile() {
		if($this->request->is('ajax')) {
			$result = array('error' => true, 'message' => '');

			if(isset($this->request->query['fileId']) && isset($this->request->query['downloadId'])) {
				$fileId = $this->request->query['fileId'];
				$downloadId = $this->request->query['downloadId'];

				if($file = $this->Download->DownloadFile->findByIdAndDownloadId($fileId, $downloadId)) {
					$result['error'] = false;

					## mark all files, for this download, as inactive
					$this->Download->DownloadFile->updateAll(array('DownloadFile.active' => false), array('DownloadFile.download_id' => $downloadId));

					$this->Download->DownloadFile->id = $fileId;
					$this->Download->DownloadFile->saveField('active', true);
				} else {
					$result['message'] = __('Invalid Request');
				}
			}
			return new CakeResponse(array('body' => json_encode($result)));
		}

		return null;
	}
}