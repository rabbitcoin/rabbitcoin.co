<?php

/**
 * Class HomeController
 *
 * @property Link $Link
 * @property Download $Download
 */
class HomeController extends AppController {
	var $uses = array('Link', 'Download', 'Blog');
	var $components = array('RequestHandler');
	var $helpers = array('Text');
	var $paginate = array(
		'Blog' => array(
			'order' => array(
				'Blog.publish_date' => 'DESC'
			),
			'conditions' => array(
				'Blog.publish_date <= NOW()'
			)
		)
	);

	public function beforeFilter() {
		parent::beforeFilter();
	}

	public function about() {
		$this->set('title_for_layout', 'About');
	}

	public function cbe() {
		$this->set('title_for_layout', 'CBE');
	}

	public function contact() {
		$this->set('title_for_layout', 'Contact Us');
	}

	public function index() {
		$this->set('title_for_layout', 'Welcome');
		$this->set('file_windows', $this->Download->getActiveDownload('windows'));
		$this->set('file_mac', $this->Download->getActiveDownload('macintosh'));
		$this->set('file_source', $this->Download->getActiveDownload('source'));
		
		$this->set('launch_time', '2014/02/19 13:00:00 EST');
	}

	public function links() {
		$this->set('title_for_layout', 'Links');

		$links = $this->Link->find('all', array(
			'order' => array(
				'LinkGroup.title', 'Link.title'
			),
			'fields' => array(
				'Link.href', 'Link.title', 'LinkGroup.title'
			)
		));

		$this->set('links', $links);
	}

	public function team() {
		$this->set('title_for_layout', 'Our Team');
	}

	public function blog() {
		if($this->RequestHandler->isRss()) {
			$this->set('title_for_layout', 'RabbitCoin RSS');

			$blogs = $this->Blog->find('all', array(
				'limit' => 20,
				'order' => array(
					'Blog.publish_date' => 'DESC'
				),
				'conditions' => array(
					'Blog.publish_date <= NOW()'
				),
				'recursive' => -1
			));

			return $this->set(compact('blogs'));
		}

		$this->set('title_for_layout', 'Blog');
		$this->set('blogs', $this->Paginate('Blog'));
		$this->set('rss', sprintf('%s.rss', Router::url($this->here, true)));
	}

	public function blog_detail($id) {
		$this->Blog->id = $id;
		if(!$this->Blog->exists()) {
			throw new NotFoundException(__('Blog Entry Not Found'));
		}

		$blog = $this->Blog->read();

		$this->set('title_for_layout', 'Blog - ' . $blog['Blog']['title']);
		$this->set('blog', $blog);
	}
    
}