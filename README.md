rabbitcoi.in [RBBT] based on the CakePHP Framework
=======

[http://rabbitco.in/](http://rabbitco.in/)

![CorgiCoin](http://i.imgur.com/POAtUZV.png)

## Website Contributions

We welcome all contributions to our website.

## About CakePHP

CakePHP is a rapid development framework for PHP which uses commonly known design patterns like Active Record, Association Data Mapping, Front Controller and MVC.
Our primary goal is to provide a structured framework that enables PHP users at all levels to rapidly develop robust web applications, without any loss to flexibility.

[http://cakephp.org/](http://cakephp.org/)
